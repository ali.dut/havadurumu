<?php

use App\Http\Controllers\HavaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('hava')->group(function () {
    Route::get('/', [HavaController::class, 'index']);
    Route::post('/', [HavaController::class, 'store']);
    Route::post('/search', [HavaController::class, 'search']);
    Route::get('/{id}', [HavaController::class, 'get']);
    Route::put('/{id}', [HavaController::class, 'update']);
    Route::patch('/{id}', [HavaController::class, 'save']);
    Route::delete('/{id}', [HavaController::class, 'delete']);
});

/*
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});*/
