<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <style>
        .container-fluid {
            width: 50%;
            margin-top: 100px;
        }
    </style>
</head>
<body class="antialiased">

<script>
    window.onload = function () {
        $.get("http://ipinfo.io", function (response) {
            var city = response.city;
            var loc = response.loc.split(",");

            $('#text1').val(loc[0]);
            $('#text2').val(loc[1]);
            $('#text3').val(city);
            $.post("/api/v1/hava", {name: city, lat: loc[0], long: loc[1], details: null})
                .done(function (data) {
                    console.log("Data Loaded: ", data);
                });
        }, "jsonp");

        setInterval(function () {
            var city = $('#text3').val();
            $.post("/api/v1/hava/search", {search: city, lat: null, long: null}, function (data) {
                var res = "";
                var asdf = data.data.hava;
                for (var r in asdf) {

                    var val = asdf[r];
                    valu = ($.parseJSON(val["details"]));
                    res += "Derece : " + valu.derece + " Tarih :" + valu.time + " <br> ";
                }
                $('#history').html(res);

            })
        }, 1000);
    }


</script>
<div class="container-fluid">
    <form>
        <div class="form-group">
            <label for="text1">Lat</label>
            <input type="text" class="form-control" id="text1" placeholder="lat">
        </div>
        <div class="form-group">
            <label for="text2">Long</label>
            <input type="text" class="form-control" id="text2" placeholder="long">
        </div>
        <div class="form-group">
            <label for="text3">Şehir</label>
            <input type="text" class="form-control" id="text3" placeholder="city">
        </div>


    </form>
    <br>
    <br>
    <br>
    <div id="history">

    </div>
</div>

</body>
</html>
