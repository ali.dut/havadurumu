<?php

namespace App\Queries;

class HavaQueries
{
    const SQL_search = <<<SQL
    SELECT
        h.id as id,
        h.lat as lat,
        h.long as long,
        h.name as name,
        hd.details as details,
        hd.date as date
    FROM
            public.tb_hava h
        JOIN
            public.tb_hava_details hd
        ON h.id = hd.hava_id
    WHERE
        (
            ( '' = :search OR :search IS NULL) OR (h.name like '%' || :search || '%')
        )
        AND
        (
            (  ( 0 = :lat  OR :lat IS NULL)  OR h.lat = :lat)
            OR
            (  ( 0 = :long  OR :long IS NULL)  OR h.long = :long)
        )
    SQL;

    const SQL_searchDefaultParams = [
        "search" => "",
        "lat" => 0,
        "long" => 0,
    ];

}
