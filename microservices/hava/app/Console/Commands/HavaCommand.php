<?php

namespace App\Console\Commands;

use App\Helpers\TurkeyWeather;
use Illuminate\Console\Command;

class HavaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:hava';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->dongu();
        return 0;
    }

    public function dongu()
    {
        $list = $this->getHavaList();
        foreach ($list as $item) {
            $time = date("Y-m-d H:i:s");
            $weather = new TurkeyWeather();
            $weather->province($item->name);
            $obj = new \stdClass();
            $obj->name = $item->name;
            $obj->derece = $weather->temperature();
            $obj->time = $time;
            $json = json_encode($obj);

            $this->save($item->id, $json, $time);
        }
    }

    public function getHavaList()
    {
        return \DB::select("SELECT id,name FROM public.tb_hava");
    }

    public function save($id, $details, $time)
    {
        \DB::table("public.tb_hava_details")->insert([
            'hava_id' => $id,
            'details' => $details,
            'date' => $time
        ]);
    }

}
