<?php

namespace App\Http\Controllers;

use App\Services\HavaService;
use Illuminate\Http\Request;

class HavaController extends Controller
{
    public function index(HavaService $service)
    {
        $response = $service->index();

        return response()->json($response);
    }

    public function store(HavaService $service, Request $request)
    {
        $response = $service->store($request->all());

        return response()->json($response);
    }

    public function search(HavaService $service, Request $request)
    {
        $response = $service->search($request->all());

        return response()->json($response);
    }

    public function get($id, HavaService $service)
    {
        $response = $service->get((int)$id);

        return response()->json($response);
    }

    public function update(Request $request, $id, HavaService $service)
    {
        $response = $service->update($request->all(), (int)$id);

        return response()->json($response);
    }

    public function save(Request $request, HavaService $service, $id = 0)
    {
        $response = $service->save($request->all(), (int)$id);

        return response()->json($response);
    }

    public function delete($id, HavaService $service)
    {
        $response = $service->delete((int)$id);

        return response()->json($response);
    }

}
