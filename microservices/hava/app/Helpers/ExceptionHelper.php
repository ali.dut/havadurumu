<?php

namespace App\Helpers;


class ExceptionHelper
{
    public static function createException($message, $status, $response)
    {
        $exception = new \Exception($message, $status);
        $exception->_response = $response;

        return $exception;
    }
}
