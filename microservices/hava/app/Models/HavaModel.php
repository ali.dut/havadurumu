<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HavaModel extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'public.tb_hava';
    protected $dateFormat = 'U';
    public $timestamps = false;
    public bool $afterCommit = true;
    protected $attributes = [];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'lat',
        'long',
        'name',
        'details',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'name' => 'string',
        'details' => 'array',
    ];

}
