<?php

namespace App\Services;

use App\Classes\Hava;
use App\Helpers\ExceptionHelper;
use App\Helpers\ServiceResponse;
use App\Models\HavaModel;
use App\Queries\HavaQueries;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;


class HavaService extends BaseService
{

    public function index(): ServiceResponse
    {
        $hava = HavaModel::all();

        return $this->setResponse(200, 'success', ['hava' => $hava->toArray()]);
    }

    public function store(array $data): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->havaRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $this->validateParams($data);

            $havaCheck = HavaModel::where('name', '=', $data['name'])->first();

            if (!empty($havaCheck)) throw ExceptionHelper::createException('Bu isim sistemde zaten kayıtlı', 400, null);

            $hava = new Hava();
            $hava->store($data);

            \DB::commit();
            return $this->setResponse(200, 'success', ['hava' => $hava->model->toArray()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    public function get(int $id = 0): ServiceResponse
    {
        $hava = HavaModel::find($id);

        return empty($hava) ? $this->setResponse(204, 'Hava not found', null) :
            $this->setResponse(200, 'success', ['hava' => $hava->toArray()]);
    }

    public function search(array $params): ServiceResponse
    {
        try {
            $params = array_merge(HavaQueries::SQL_searchDefaultParams, $params);
            $validator = Validator::make($params, $this->searchParamsRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $hava = \DB::select(HavaQueries::SQL_search, $params);

            return $this->setResponse(200, 'success', [
                'hava' => $hava
            ]);
        } catch (\Exception $e) {
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }

    }

    public function update(array $data, int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->havaRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $this->validateParams($data);
            $hava = new Hava($id);

            if (empty($hava->model->id)) {
                $havaCheck = HavaModel::where("name", "=", $data['name'])->first();
                if (empty($havaCheck)) throw ExceptionHelper::createException('Kayıt bulunamadı', 400, null);

                $hava = new Hava($havaCheck['id']);
            }
            $hava->update($data);
            \DB::commit();
            return $this->setResponse(200, 'success', ['hava' => $hava->model->toArray()]);

        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    public function save(array $data, int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->havaRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);
            $this->validateParams($data);
            $hava = new Hava($id);

            if (empty($hava->model->id)) {
                $HavaModel = HavaModel::where('name', '=', $data['name'])->first();
                if (!empty($HavaModel)) $hava = new Hava($HavaModel->id);
            }

            $hava->save($data);
            \DB::commit();
            return $this->setResponse(200, 'success', ['hava' => $hava->model->toArray()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    public function delete(int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $count = HavaModel::destroy($id);
            \DB::commit();
            return $this->setResponse(200, 'success', ['count' => $count]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    public function havaRecordRules(): array
    {
        return [
            'lat' => 'required|numeric',
            'long' => 'required|numeric',
            'name' => 'required|string',
            'details' => 'nullable|array'
        ];
    }

    public function searchParamsRules(): array
    {
        return [
            'lat' => 'numeric|nullable',
            'long' => 'numeric|nullable',
            'search' => 'string|nullable'
        ];
    }


    public function validateParams($data)
    {
        /*
         * Kontroller yapılacak
         */

    }

}
