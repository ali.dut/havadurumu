<?php

namespace App\Classes;

use App\Models\HavaModel;

class Hava
{
    public $model;

    public function __construct(int $id = 0)
    {
        $this->model = empty($id) ? null : HavaModel::find($id);
        $this->model = empty($this->model) ? new HavaModel() : $this->model;
    }

    public function save(array $data = null, int $id = 0)
    {
        if (empty($this->model->id)) return $this->store($data);

        return $this->update($data);
    }

    public function update(array $data = null)
    {
        $this->setModel($data);
        $this->model->update();

        return $this->model;
    }

    public function store(array $data = null)
    {
        $this->setModel($data);
        $this->model->save();

        return $this->model;
    }

    public function setModel(array $data = null)
    {
        $this->model->lat = $data['lat'];
        $this->model->long = $data['long'];
        $this->model->name = $data['name'];
        $this->model->details = $data['details'];
    }
}
